import { writable } from "svelte/store";
import type { IssLocation } from "./issLocation";

export const locationStore = writable<IssLocation>({lat: 0, lon: 0})