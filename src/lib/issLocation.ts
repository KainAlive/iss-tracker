export type IssLocation = {
    lat: number,
    lon: number
}

type ApiResponse = {
    message: string,
    timestamp: number,
    iss_position: {
        latitude: number,
        longitude: number
    }
}

const API_URL = "http://api.open-notify.org/iss-now.json"

export async function getLocation(): Promise<IssLocation> {
    let resp = await fetch(API_URL)
    let data: ApiResponse = await resp.json()
    console.log(data)
    let location: IssLocation = {
        lat: data.iss_position.latitude,
        lon: data.iss_position.longitude
    }

    return location
}